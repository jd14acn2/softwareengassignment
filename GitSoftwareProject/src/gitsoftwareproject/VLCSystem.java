package gitsoftwareproject;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;
import java.util.Calendar;

/**
 *
 * @author Admin
 */
public class VLCSystem {
    
    public HashMap<Room, HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>>> roomTimetables = 
            new HashMap<Room, HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>>>();
    
    public HashMap<String, Coach> coachList = new HashMap<String, Coach>();
    public HashMap<String, Member> memberList = new HashMap<String, Member>();
    
    public Calendar startTime = Calendar.getInstance();
    
    public VLCSystem(){
        
        CreateRoomTimeTables();
    }
    
    public enum Room
    {
        POOL, GYM, STUDIO_A, STUDIO_B, STUDIO_C, INDOOR_COURTS, OUTDOOR_COURTS;
    }
    
    enum Expertise
    {
        BADMINTON, SWIMMING, DANCE, SPINNING, FOOTBALL, TENNIS, YOGA, CIRCUIT_TRAINING;
    }
    
    private void CreateRoomTimeTables(){
        
        //Create day map to be later populated with classes
        
        HashMap<Date, ExerciseClass> MONDAY = new HashMap<Date, ExerciseClass>();
        HashMap<Date, ExerciseClass> TUESDAY = new HashMap<Date, ExerciseClass>();
        HashMap<Date, ExerciseClass> WEDNESDAY = new HashMap<Date, ExerciseClass>();
        HashMap<Date, ExerciseClass> THURSDAY = new HashMap<Date, ExerciseClass>();
        HashMap<Date, ExerciseClass> FRIDAY = new HashMap<Date, ExerciseClass>();
        HashMap<Date, ExerciseClass> SATURDAY = new HashMap<Date, ExerciseClass>();
        HashMap<Date, ExerciseClass> SUNDAY = new HashMap<Date, ExerciseClass>();
        
        HashMap<String, HashMap<Date, ExerciseClass>> dayList = new HashMap<String, HashMap<Date, ExerciseClass>>();
        
        dayList.put("MONDAY", MONDAY);
        dayList.put("TUESDAY", TUESDAY);
        dayList.put("WEDNESDAY", WEDNESDAY);
        dayList.put("THURSDAY", THURSDAY);
        dayList.put("FRIDAY", FRIDAY);
        dayList.put("SATURDAY", SATURDAY);
        dayList.put("SUNDAY", SUNDAY);
        
        // Create Week map
        
        HashMap<String, HashMap<Date, ExerciseClass>> Week1 = new HashMap<String, HashMap<Date, ExerciseClass>>();
        HashMap<String, HashMap<Date, ExerciseClass>> Week2 = new HashMap<String, HashMap<Date, ExerciseClass>>();
        HashMap<String, HashMap<Date, ExerciseClass>> Week3 = new HashMap<String, HashMap<Date, ExerciseClass>>();
        HashMap<String, HashMap<Date, ExerciseClass>> Week4 = new HashMap<String, HashMap<Date, ExerciseClass>>();
        HashMap<String, HashMap<Date, ExerciseClass>> Week5 = new HashMap<String, HashMap<Date, ExerciseClass>>();
        HashMap<String, HashMap<Date, ExerciseClass>> Week6 = new HashMap<String, HashMap<Date, ExerciseClass>>();
        HashMap<String, HashMap<Date, ExerciseClass>> Week7 = new HashMap<String, HashMap<Date, ExerciseClass>>();
        HashMap<String, HashMap<Date, ExerciseClass>> Week8 = new HashMap<String, HashMap<Date, ExerciseClass>>();
        
        HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>> weekList = new HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>>();
        
        weekList.put("Week1", Week1);
        weekList.put("Week2", Week2);
        weekList.put("Week3", Week3);
        weekList.put("Week4", Week4);
        weekList.put("Week5", Week5);
        weekList.put("Week6", Week6);
        weekList.put("Week7", Week7);
        weekList.put("Week8", Week8);
        
        // Create Room map

        HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>> POOL_TimeTable = new HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>>();
        HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>> GYM_TimeTable = new HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>>();
        HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>> STUDIO_A_TimeTable = new HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>>();
        HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>> STUDIO_B_TimeTable = new HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>>();
        HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>> STUDIO_C_TimeTable = new HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>>();
        HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>> INDOOR_COURTS_TimeTable = new HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>>();
        HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>> OUTDOOR_COURTS_TimeTable = new HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>>();
        
  
        roomTimetables.put(Room.POOL, POOL_TimeTable);
        roomTimetables.put(Room.GYM, GYM_TimeTable);
        roomTimetables.put(Room.STUDIO_A, STUDIO_A_TimeTable);
        roomTimetables.put(Room.STUDIO_B, STUDIO_B_TimeTable);
        roomTimetables.put(Room.STUDIO_C, STUDIO_C_TimeTable);
        roomTimetables.put(Room.INDOOR_COURTS, INDOOR_COURTS_TimeTable);
        roomTimetables.put(Room.OUTDOOR_COURTS, OUTDOOR_COURTS_TimeTable);
        
        PopulateTimeTables(dayList, weekList);
 
    }
    
    private void PopulateTimeTables(HashMap<String, HashMap<Date, ExerciseClass>> dayList, HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>> weekList){
        
        
        // For each of the 8 weeks, add the 7 days
        for(Map.Entry<String, HashMap<String, HashMap<Date, ExerciseClass>>> weekEntry : weekList.entrySet()) 
        {
            String weekKey = weekEntry.getKey();
            
            for(Map.Entry<String, HashMap<Date, ExerciseClass>> dayEntry : dayList.entrySet()) 
            {
                String dayKey = dayEntry.getKey();
                HashMap dayVal = dayEntry.getValue();
                weekList.get(weekKey).put(dayKey, dayVal);
            }
        }
        
        // For each of the rooms, add all 8 of the weeks
        for(Map.Entry<Room, HashMap<String, HashMap<String, HashMap<Date, ExerciseClass>>>> roomEntry : roomTimetables.entrySet())
        {
            Room roomKey = roomEntry.getKey();
            
            for(Map.Entry<String, HashMap<String, HashMap<Date, ExerciseClass>>> weekEntry : weekList.entrySet()) 
            {
                String weekKey = weekEntry.getKey();
                HashMap weekVal = weekEntry.getValue();
                roomTimetables.get(roomKey).put(weekKey, weekVal);
            }
        }
    }
    
    public void LoadSampleCoaches(){
        Coach coachBerry = new Coach("D_Berry", "Dave Berry", "1 Roman Road, Castle Street, Worcester WR13 AAA", "01880 888 808");
        Coach coachWilliams = new Coach("G_Williams", "Gareth Williams", "Old Farm House, Plough Place, Gloucester GL3 BBB", "01234 567890");
        Coach coachHenry = new Coach("D_Henry", "Denise Henry", "36 Family Home, CulDeSac Ave, Malvern WR14 CCC", "01680 89 89 89");
        Coach coachBlackwood = new Coach("M_Blackwood", "Mike Blackwood", "1150 Posh Appartment, Main Street, Worcester WR1 DDD", "08005426969");
        Coach coachJenkins = new Coach("C_Jenkins", "Catherine Jenkins", "The Wheat Mill, Sleepy Village, Hereford HE01 EEE", "07500 500500");
        
        coachBerry.addExpertise(Expertise.TENNIS);
        coachWilliams.addExpertise(Expertise.FOOTBALL);
        coachHenry.addExpertise(Expertise.DANCE);
        coachHenry.addExpertise(Expertise.YOGA);
        coachBlackwood.addExpertise(Expertise.CIRCUIT_TRAINING);
        coachBlackwood.addExpertise(Expertise.DANCE);
        coachJenkins.addExpertise(Expertise.BADMINTON);
        
        coachList.put(coachBerry.getUserName(), coachBerry);
        coachList.put(coachWilliams.getUserName(), coachWilliams);
        coachList.put(coachHenry.getUserName(), coachHenry);
        coachList.put(coachBlackwood.getUserName(), coachBlackwood);
        coachList.put(coachJenkins.getUserName(), coachJenkins);
        
    }
    
    public void LoadSampleClasses(){
        Calendar endTime = Calendar.getInstance();
        
        startTime.set(2000, 5, 26, 18, 00, 00);
        endTime.set(2000, 5, 26, 19, 00, 00);
        coachList.get("D_Berry").createClass("Beginners Tennis", Room.OUTDOOR_COURTS, 5, startTime.getTime(), endTime.getTime(), "MONDAY", Expertise.TENNIS);
        
        startTime.set(2000, 5, 26, 19, 00, 00);
        endTime.set(2000, 5, 26, 20, 00, 00);
        coachList.get("G_Williams").createClass("5 a Side Football", Room.INDOOR_COURTS, 10, startTime.getTime(), endTime.getTime(), "MONDAY", Expertise.FOOTBALL);
        
        startTime.set(2000, 5, 26, 14, 30, 00);
        endTime.set(2000, 5, 26, 15, 30, 00);
        coachList.get("M_Blackwood").createClass("Dance 2 Da Beat", Room.STUDIO_B, 15, startTime.getTime(), endTime.getTime(), "MONDAY", Expertise.DANCE);
        
        startTime.set(2000, 5, 27, 17, 00, 00);
        endTime.set(2000, 5, 27, 18, 00, 00);
        coachList.get("D_Henry").createClass("Hot Yoga", Room.STUDIO_A, 6, startTime.getTime(), endTime.getTime(), "TUESDAY", Expertise.YOGA);
        
        startTime.set(2000, 5, 28, 19, 00, 00);
        endTime.set(2000, 5, 28, 20, 00, 00);
        coachList.get("D_Berry").createClass("Beginners Tennis", Room.OUTDOOR_COURTS, 5, startTime.getTime(), endTime.getTime(), "WEDNESDAY", Expertise.TENNIS);
        
        startTime.set(2000, 5, 29, 14, 00, 00);
        endTime.set(2000, 5, 29, 15, 00, 00);
        coachList.get("D_Henry").createClass("Cold Yoga", Room.STUDIO_A, 3, startTime.getTime(), endTime.getTime(), "THURSDAY", Expertise.YOGA);
        
        startTime.set(2000, 5, 30, 16, 00, 00);
        endTime.set(2000, 5, 30, 17, 00, 00);
        coachList.get("M_Blackwood").createClass("Circuits", Room.GYM, 8, startTime.getTime(), endTime.getTime(), "FRIDAY", Expertise.CIRCUIT_TRAINING);
        
        startTime.set(2000, 5, 31, 10, 00, 00);
        endTime.set(2000, 5, 31, 11, 00, 00);
        coachList.get("D_Henry").createClass("Dance 2 Da Beat", Room.STUDIO_B, 15, startTime.getTime(), endTime.getTime(), "SATURDAY", Expertise.DANCE);
        
        startTime.set(2000, 6, 1, 11, 00, 00);
        endTime.set(2000, 6, 1, 12, 00, 00);
        coachList.get("C_Jenkins").createClass("All Play Badminton", Room.INDOOR_COURTS, 6, startTime.getTime(), endTime.getTime(), "SUNDAY", Expertise.BADMINTON);
        
        startTime.set(2000, 6, 1, 12, 00, 00);
        endTime.set(2000, 6, 1, 13, 00, 00);
        coachList.get("C_Jenkins").createClass("Advanced Badminton", Room.INDOOR_COURTS, 1, startTime.getTime(), endTime.getTime(), "SUNDAY", Expertise.BADMINTON);
    
    }
    
    public void LoadSampleMembers(){
        Member memberPhillips = new Member("C_Phillips", "Chris Phillips", "The Wheat Mill, Sleepy Village, Hereford HE01 EEE", "07500 500500");
        Member memberRogers = new Member("R_Rogers", "Roger Rogers", "The Wheat Mill, Sleepy Village, Hereford HE01 EEE", "07500 500500");
        Member memberHulk = new Member("I_Hulk", "Incredible Hulk", "The Wheat Mill, Sleepy Village, Hereford HE01 EEE", "07500 500500");
        Member memberIronMan = new Member("I_Man", "Tony Stark", "The Wheat Mill, Sleepy Village, Hereford HE01 EEE", "07500 500500");
        Member memberBlackWidow = new Member("B_Widow", "Black Widow", "The Wheat Mill, Sleepy Village, Hereford HE01 EEE", "07500 500500");
        Member memberThor = new Member("M_Thor", "Mighty Thor", "The Wheat Mill, Sleepy Village, Hereford HE01 EEE", "07500 500500");
        Member memberSnow = new Member("J_Snow", "Jon Snow", "The Wheat Mill, Sleepy Village, Hereford HE01 EEE", "07500 500500");
        Member memberSkywalker = new Member("L_Skywalker", "Luke_Skywalker", "The Wheat Mill, Sleepy Village, Hereford HE01 EEE", "07500 500500");
        Member memberBanister = new Member("R_Banister", "Roger Banister", "The Wheat Mill, Sleepy Village, Hereford HE01 EEE", "07500 500500");
        Member memberOwen = new Member("U_Owen", "Uncle Owen", "The Wheat Mill, Sleepy Village, Hereford HE01 EEE", "07500 500500");
        
        
        memberList.put(memberPhillips.getUserName(), memberPhillips);      
        memberList.put(memberRogers.getUserName(), memberPhillips);  
        memberList.put(memberHulk.getUserName(), memberPhillips);  
        memberList.put(memberIronMan.getUserName(), memberPhillips);  
        memberList.put(memberBlackWidow.getUserName(), memberPhillips);  
        memberList.put(memberThor.getUserName(), memberPhillips);  
        memberList.put(memberSnow.getUserName(), memberPhillips);  
        memberList.put(memberSkywalker.getUserName(), memberPhillips);  
        memberList.put(memberBanister.getUserName(), memberPhillips);  
        memberList.put(memberOwen.getUserName(), memberPhillips);  
    }
    
   
    public void LoadSampleClassBookings(){
        
        startTime.set(2000, 5, 26, 18, 00, 00);
        Member member = memberList.get("C_Phillips");
        member.bookClass(roomTimetables.get(Room.OUTDOOR_COURTS).get("Week1").get("MONDAY").get(startTime.getTime()), member);
    }
    

    public class User {
        private String userId;
        private String fullName;
        private String address;
        private String telephone;
        
        public User(String userId, String fullName, String address, String telephone){
            this.userId = userId;
            this.fullName =  fullName;
            this.address = address;
            this.telephone = telephone;
        }
        
        public String getUserName() {
            return this.userId;
        }
    }
    
    class Member extends User {
        
        public Member(String userId, String fullName, String address, String telephone) {
            super(userId, fullName, address, telephone);
        }
        
        public boolean bookClass(ExerciseClass eClass, Member member)
        {
            eClass.addNameToAttendence(member);
            return true;
        }
        
        public boolean cancelClassBooking()
        {
            return true;
        }
        
        public boolean searchExpertise(String expertiseName)
        {
            return true;
        }
        
        public boolean searchCoach(String coachName)
        {
            return true;
        } 
        
    }
    
    class Coach extends User {
          
        private ArrayList<Expertise> expertiseList = new ArrayList<Expertise>();
        
        public Coach(String userId, String fullName, String address, String telephone) {
            super(userId, fullName, address, telephone);
        }
        
        public boolean createClass(String className, Room room, int capacity, Date startTime, Date endTime, String day, Expertise expertise)
        {
            for (int i = 1; i < 9; i++){
                String weekNum = "Week" + i;
                ExerciseClass eClass = new ExerciseClass(className, room, capacity,  startTime, endTime, day, expertise);
                roomTimetables.get(room).get(weekNum).get(day).put(startTime, eClass);
            }
            return true;
        }
        
        public boolean createAppointmentAvailability()
        {
            return true;
        }
        
        public void addExpertise(Expertise expertise)
        {
            expertiseList.add(expertise);
        }
        
    }
    
    public class ExerciseClass {
        private String className;
        private Room room;
        private int capacity;
        private Date startTime;
        private Date endTime;
        private String day;

        private Expertise expertise;
        private ArrayList attendenceList;
        
        
        public ExerciseClass(String className, Room room, int capacity, Date startTime, Date endTime, String day, Expertise expertise){
            this.className = className;
            this.room =  room;
            this.capacity = capacity;
            this.startTime = startTime;
            this.endTime = endTime;
            this.day = day;
            this.expertise = expertise;
            this.attendenceList = new ArrayList<Member>(capacity);
        }
        
        public boolean addNameToAttendence(Member member){
            if (attendenceList.size() < capacity){
                if (!attendenceList.contains(member)){
                    attendenceList.add(member);
                    return true;
                }
            }
            return false;
        }
        
        public ArrayList getAttendenceList() {
            return this.attendenceList;
        }
    }
    
    
    
}
