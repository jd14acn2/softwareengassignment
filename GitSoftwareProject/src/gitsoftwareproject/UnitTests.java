/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsoftwareproject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.Calendar;
import static org.junit.Assert.*;


/**
 *
 * @author Admin
 */
public class UnitTests {
    
    @Test
    public void CheckBookingsAddMemberToAttendence() {
        VLCSystem vlc = new VLCSystem();

        // assert statements
        Calendar testingStartTime = vlc.startTime;
        testingStartTime.set(2000, 5, 26, 18, 00, 00);
        
        VLCSystem.Member testMember = vlc.memberList.get("C_Phillips");
        VLCSystem.ExerciseClass testClass = vlc.roomTimetables.get(VLCSystem.Room.OUTDOOR_COURTS).get("Week1").get("MONDAY").get(testingStartTime.getTime());
        
        testMember.bookClass(testClass, testMember);
        
        assertTrue(testClass.getAttendenceList().contains(testMember));
    }
    
    @Test
    public void CheckCapacityNotExceeded() {
        VLCSystem vlc = new VLCSystem();

        // assert statements
        Calendar testingStartTime = vlc.startTime;
        testingStartTime.set(2000, 6, 1, 12, 00, 00);
        
        VLCSystem.Member testMember1 = vlc.memberList.get("C_Phillips");
        VLCSystem.ExerciseClass testClass = vlc.roomTimetables.get(VLCSystem.Room.INDOOR_COURTS).get("Week2").get("SUNDAY").get(testingStartTime.getTime());
        
        testMember1.bookClass(testClass, testMember1);
        
        VLCSystem.Member testMember2 = vlc.memberList.get("I_Man");
        
        testMember2.bookClass(testClass, testMember2);
        
        assertFalse(testMember2.bookClass(testClass, testMember2));
        assertFalse(testClass.getAttendenceList().contains(testMember2));
    }
    
    @Test
    public void CheckClassAddedToTimetable() {
        VLCSystem vlc = new VLCSystem();

        // assert statements
        Calendar testingStartTime = vlc.startTime;
        Calendar testingEndTime = Calendar.getInstance();
        testingStartTime.set(2000, 5, 26, 10, 00, 00);
        testingEndTime.set(2000, 5, 26, 11, 00, 00);
        
        VLCSystem.Coach testCoach = vlc.coachList.get("D_Berry");
        VLCSystem.ExerciseClass testingClass = testCoach.createClass("Beginners Tennis", VLCSystem.Room.OUTDOOR_COURTS, 5, testingStartTime.getTime(), testingEndTime.getTime(), "MONDAY", VLCSystem.Expertise.TENNIS);
        
        //assertEquals(vlc.roomTimetables.get(VLCSystem.Room.OUTDOOR_COURTS).get("Week2").get("MONDAY").get(testingStartTime.getTime()), 
        
    }
    
    @Test
    public void CheckExpertiseAddedToCoach() {
        VLCSystem vlc = new VLCSystem();
        
        VLCSystem.Coach testCoach = vlc.coachList.get("D_Berry");
        testCoach.addExpertise(VLCSystem.Expertise.DANCE);
        assertTrue(testCoach.getExpertiseList().contains(VLCSystem.Expertise.DANCE));
}
